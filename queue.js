let collection = [];

// Write the queue functions below.
// console.log(collection);

//Number 1
function print(){
    console.log(`The printed value is an empty array. RESULT = ${collection}`)
    return collection
}
// print()

//Number 2 and 3 and 5 and 6
function enqueue(newElement){
    collection.push(newElement)
    console.log(`The value has been enqueued. RESULT = ${collection}`)
    return collection
}
// enqueue(`John`)
// enqueue(`Jane`)

//Number 4
function dequeue(){
    collection.shift()
    console.log(`The value has been dequeued. RESULT = ${collection}`)
    return collection
}
// dequeue()
// enqueue(`Bob`)
// enqueue(`Cherry`)
//Number 7

function front(){
    console.log(`The first value has been retrieved. RESULT = ${collection[0]}`)
    return collection[0]
}
// front()
//Number 8
function size(){
    console.log(`The size of the queue has been retrieved. RESULT = ${collection.length}`)
    return collection.length
}
// size()
//Number 9
function isEmpty(){
    if(collection.length == 0){
        console.log (`The result has been retrieved. RESULT = true`)
        return true
    }else{
        console.log (`The result has been retrieved. RESULT = false`)
        return false
    } 
}
// isEmpty()
// collection.push(`John`)




module.exports = {
    print,
    enqueue,
    dequeue,
    size,
    isEmpty,
    front
};